/*
  Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- Візьміть виконане домашнє завдання номер 4 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:

    1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
    2. Створити метод getAge() який повертатиме скільки користувачеві років.
    3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, 
    з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.


- Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/
"use strict";
const getUserNameWithPrompt = function () {
  let firstName = "";
  let lastName = "";

  do {
    firstName = prompt("Input First Name", firstName) ?? "";
    lastName = prompt("Input Last Name", lastName) ?? "";
  } while (!firstName.trim() || !lastName.trim());
  return { firstName, lastName };
};

const parseDate = (dateToParse) => {
  const [day, month, year] = dateToParse.split(".");
  if (!day || !month || !year || year.length != 4) return null;
  const isoDate = `${year}-${month}-${day}T00:00:00`;
  const timeStamp = Date.parse(isoDate);
  return !isNaN(timeStamp) ? new Date(timeStamp) : null;
};

const getBirthday = () => {
  let birthday = "";
  while (true) {
    birthday =
      prompt('Please Enter Birthday Date in format "dd.mm.yyyy"', birthday) ??
      "";
    const date = parseDate(birthday);
    if (date && date <= new Date()) return date;
  }
};

const createNewUser = function () {
  const newUser = getUserNameWithPrompt();
  const birthday = getBirthday();
  Object.defineProperty(newUser, "firstName", {
    writable: false,
  });
  Object.defineProperty(newUser, "lastName", {
    writable: false,
  });
  Object.defineProperty(newUser, "birthday", {
    value: birthday,
    writable: false,
    configurable: true,
  });
  newUser.setLastName = function (lastName) {
    Object.defineProperty(this, "lastName", { value: lastName });
  };
  newUser.setFirstName = function (firstName) {
    Object.defineProperty(this, "firstName", { value: firstName });
  };
  newUser.getLogin = function () {
    return `${this.firstName[0]}${this.lastName}`.toLowerCase();
  };

  newUser.getAge = function () {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const age = today.getFullYear() - this.birthday.getFullYear();
    return new Date(this.birthday).setYear(today.getFullYear()) - today > 0
      ? age - 1
      : age;
  };
  newUser.getPassword = function () {
    return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
  };
  return newUser;
};

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

// TEST CASES

// const testCases = () => {
//   console.log(parseDate("11.12.1999"));
//   console.log(parseDate("11.13.1999"));
//   console.log(parseDate("11.12"));
//   console.log(parseDate("11.12.99"));
//   console.log(parseDate("16.12.2022"));
//   const testObject = createNewUser();
//   console.log(testObject);
//   try {
//     testObject.firstName = "Gheka";
//   } catch (error) {
//     console.log("Try to set firstName directly");
//     console.log(error);
//   }
//   try {
//     testObject.lastName = "Gheka";
//   } catch (error) {
//     console.log("Try to set lastName directly");
//     console.log(error.message);
//   }
//   console.log(testObject);
//   testObject.setFirstName("Gheka");
//   testObject.setLastName("EmetsEmets");
//   console.log(testObject.getLogin());
//   console.log(Object.getOwnPropertyDescriptors(testObject));
//   console.log(testObject.getAge());
//   console.log(testObject.getPassword());
// };
// testCases();
